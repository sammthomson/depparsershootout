package controllers

import play.api.mvc.{Action, Controller}
import play.api.libs.json.Json.toJson

object Application extends Controller {
  val stanford = Stanford.defaultInstance
  val malt = Malt.defaultInstance

  def index(sentence: String) = Action {
    Ok(views.html.index(sentence))
  }

  def parse(sentence: String) = Action {
    val stanfordParses = stanford.parseString(sentence)
    val maltParses = stanfordParses map { p => malt.parse(p.sentence) }
    Ok(toJson(Map(
      "stanfordParses" -> stanfordParses,
      "maltParses" -> maltParses
    )))
  }
}
