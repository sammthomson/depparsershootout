package controllers

import edu.stanford.nlp.pipeline.StanfordCoreNLP
import edu.stanford.nlp.pipeline.XMLOutputter
import java.io.File
import org.maltparser.MaltParserService
import scala.collection.JavaConversions._
import java.util.Properties
import scala.xml.{XML, Node}
import play.api.libs.json.Writes
import play.api.libs.json.Json.toJson
import org.maltparser.core.syntaxgraph.edge.Edge
import org.maltparser.core.syntaxgraph.DependencyStructure
import play.api.Play

case class Token(id: Int, form: String, postag: String, startChar: Int, endChar: Int) {
  def toConll = Seq(id, form, "_", "_", postag, "_", "_", "_", "_", "_").mkString("\t")
}
object Token {
  implicit val tokenWriter = new Writes[Token] {
    def writes(o: Token) = toJson(Map(
      "form" -> toJson(o.form),
      "postag" -> toJson(o.postag),
      "start" -> toJson(o.startChar),
      "end" -> toJson(o.endChar)
    ))
  }
}

case class Sentence(tokens: Seq[Token], startChar: Int, endChar: Int, text: String)

case class DependencyParse(sentence: Sentence, dependencies: Seq[(Int, Int, String)])
object DependencyParse {
  implicit val dependencyParseWrites = new Writes[DependencyParse]{
    def writes(o: DependencyParse) = toJson(Map(
      "text" -> toJson(o.sentence.text),
      "tokens" -> toJson(o.sentence.tokens.map(toJson(_))),
      "start" -> toJson(o.sentence.startChar),
      "end" -> toJson(o.sentence.endChar),
      "dependencies" -> toJson(o.dependencies.map { case (child, parent, label) =>
        toJson(Map(
          "child" -> toJson(child),
          "parent" -> toJson(parent),
          "label" -> toJson(label)
        ))
      })
    ))
  }
}

/** Wrapper for org.maltparser.MaltParserService */
class Malt(modelFile: File) {
  private val deprelField = "DEPREL"

  private[this] val processor: MaltParserService = {
    val malt = new MaltParserService()
    malt.initializeParserModel("-w %s -c %s".format(modelFile.getParentFile, modelFile.getName))
    malt
  }

  def parse(input: Sentence): DependencyParse = synchronized {
    val outputGraph = processor.parse(input.tokens.map(_.toConll).toArray)
    DependencyParse(input, getDependencies(outputGraph))
  }

  private def getDependencies(outputGraph: DependencyStructure): Seq[(Int, Int, String)] = {
    val deprelTable = outputGraph.getSymbolTables.getSymbolTable(deprelField)
    val rootLabel = outputGraph.getDefaultRootEdgeLabelSymbol(deprelTable)
    def getLabel(edge: Edge) = if (edge.hasLabel(deprelTable)) edge.getLabelSymbol(deprelTable) else rootLabel
    outputGraph.getTokenIndices.toSeq map { i =>
      val node = outputGraph.getDependencyNode(i)
      (i.toInt, node.getHead.getIndex, getLabel(node.getHeadEdge))
    }
  }
}
object Malt {
  def defaultInstance = {
    val modelFile = Play.current.configuration.getString("malt.modelfile").get
    new Malt(new File(modelFile))
  }
}

/** Wrapper for edu.stanford.nlp.pipeline.StanfordCoreNLP */
class Stanford(annotators: Traversable[String]) {
  private[this] val processor: StanfordCoreNLP = {
    val props = new Properties()
    props.setProperty("annotators", annotators.mkString(","))
    new StanfordCoreNLP(props)
  }

  def parseString(input: String): Seq[DependencyParse] = {
    val annotation = processor.process(input)
    val xml = XML.loadString(XMLOutputter.annotationToDoc(annotation, processor).toXML)
    (xml \\ "sentence") map { sentenceElt =>
      val tokens = for ((tokenElt, i) <- (sentenceElt \\ "token").zipWithIndex) yield Token(
        id = i + 1,
        form = (tokenElt \ "word").text,
        postag = (tokenElt \ "POS").text,
        startChar = (tokenElt \ "CharacterOffsetBegin").text.toInt,
        endChar = (tokenElt \ "CharacterOffsetEnd").text.toInt
      )
      val (start, end) = (tokens.head.startChar, tokens.last.endChar)
      val sentence = Sentence(tokens, start, end, input.substring(start, end))
      DependencyParse(sentence, getDependencies(sentenceElt))
    }
  }

  private def getDependencies(sentenceElt: Node, depType: String = "basic-dependencies"): Seq[(Int, Int, String)] = {
    val dependencyElements = (sentenceElt \\ "dependencies" filter { n => (n \ "@type").text == depType }) \\ "dep"
    dependencyElements map { dep =>
      ((dep \ "dependent" \ "@idx").text.toInt,
       (dep \ "governor" \ "@idx").text.toInt,
       (dep \ "@type").text)
    }
  }
}
object Stanford {
  val defaultAnnotators = Array("tokenize", "cleanxml", "ssplit", "pos", "parse")
  def defaultInstance = new Stanford(defaultAnnotators)
}
