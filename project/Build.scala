import sbt._

object ApplicationBuild extends Build {

  val appName  = "depparsershootout"

  val appVersion = "1.0-SNAPSHOT"

  val appDependencies = Seq(
    "org.maltparser" % "maltparser" % "1.7.2",
    "edu.stanford.nlp" % "stanford-corenlp" % "3.2.0",
    "edu.stanford.nlp" % "stanford-corenlp" % "3.2.0" classifier "models"
  )

  val main = play.Project(appName, appVersion, appDependencies)
}
