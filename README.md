DepParserShootout
=================

A demo to compare multiple dependency parsers.

Requires a trained model for MaltParser:
Download engmalt.linear-1.7.mco fom
http://www.maltparser.org/mco/english_parser/engmalt.html, and change
malt.modelfile to point at it in conf/application.conf.

Run with Play 2.1.0 (http://downloads.typesafe.com/play/2.1.0/play-2.1.0.zip)

    play run
