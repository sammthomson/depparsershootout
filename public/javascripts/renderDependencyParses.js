var PARSE_URL = document.location.protocol + "//" + document.location.host + document.location.pathname + "api/v1/parse",
    SPINNER_SELECTOR = "#spinner",
    DEPENDENCY_CONTAINER_SELECTOR = '#dependency_parses',
    STANFORD_CONTAINER_SELECTOR = "#stanford_container",
    STANFORD_DISPLAY_ID = "stanford_brat_parse",
    MALT_CONTAINER_SELECTOR = "#malt_container",
    MALT_DISPLAY_ID = "malt_brat_parse";

function renderDependencyParse(parseObj, containerId, displayId) {
    var bratReadyParse = convertParseToBrat(parseObj);
    $("#" + displayId).remove();
    $(containerId).append('<div id="' + displayId + '" class="dep_parse">');
    return Util.embed(displayId, {}, bratReadyParse, []);
}

function convertParseToBrat(sentence) {
    var i,
        entities = [],
        relations = [],
        tokens = [];
    for (i = 0; i < sentence.tokens.length; i++) {
        tokens.push(sentence.tokens[i].form)
    }
    for (i = 0; i < sentence.tokens.length; i++) {
        var token = sentence.tokens[i];
        entities.push([
            "T" + (i+1),
            token.postag,
            [[token.start - sentence.start, token.end - sentence.start]]
        ]);
    }
    for (i = 0; i < sentence.dependencies.length; i++) {
        var dependency = sentence.dependencies[i];
        if (dependency.parent > 0 && dependency.label != "punct") {
            relations.push([
                'R' + i,
                dependency.label,
                [
                    ['Arg1', 'T' + dependency.parent],
                    ['Arg2', 'T' + dependency.child]
                ]
            ]);
        }
    }
    return {
        text: sentence.text,
        tokens: tokens,
        entities: entities,
        relations: relations
    }
}

function submitSentence(sentence) {
    var spinner;
    spinner = $(SPINNER_SELECTOR);
    spinner.show();
    $(DEPENDENCY_CONTAINER_SELECTOR).hide();
    return $.ajax({
        url: PARSE_URL,
        data: {
            sentence: sentence
        },
        success: function(data) {
            spinner.hide();
            for (var i = 0; i < data.stanfordParses.length; i++) {
                renderDependencyParse(data.stanfordParses[i], STANFORD_CONTAINER_SELECTOR, STANFORD_DISPLAY_ID + i);
            }
            for (i = 0; i < data.maltParses.length; i++) {
                renderDependencyParse(data.maltParses[i], MALT_CONTAINER_SELECTOR, MALT_DISPLAY_ID + i);
            }
            return $(DEPENDENCY_CONTAINER_SELECTOR).show();
        },
        error: function () {
            spinner.hide();
            $(DEPENDENCY_CONTAINER_SELECTOR).text("Error");
            return $(DEPENDENCY_CONTAINER_SELECTOR).show();
        }
    });
}
